<img src="/images/pythonLogo.png" width=500>

# What is Python?
Python is an interpreted, object-oriented, high-level programming language. Its high-level built in data structures, combined with dynamic typing and dynamic binding, make it very attractive for Rapid Application Development, as well as for use as a scripting or glue language to connect existing components together. Python's simple, easy to learn syntax emphasizes readability and therefore reduces the cost of program maintenance. Python supports modules and packages, which encourages program modularity and code reuse. The Python interpreter and the extensive standard library are available in source or binary form without charge for all major platforms, and can be freely distributed.
<br />
<br />

# Compilers and Interpreters
Python is an interpreted language, and can translate written code into machine code instruction by instruction. As an interpreted language, Python code is executed in the Python VM line by line A Compiler is a program that converts that written code into machine language that the CPU can use.

JavaScript is interpreted and Java is compiled.
Software is written iteratively: write-test-fix that doen't work-test-write little more-test....

# How is Python used?
<b>1. AI and machine learning</b></br>
<i>Because Python is such a stable, flexible, and simple programming language, it’s perfect for various machine learning and artificial intelligence projects.</i>

<b>2. Data analytics<br></b>
<i>Much like AI and machine learning, data analytics is another rapidly developing field that utilises Python programming.</i>

<b>3. Data visualization<br></b>
<i>Data visualization is another popular and developing area of interest. Again, it plays into many of the strengths of Python. As well as its flexibility and the fact it’s open-source, Python provides a variety of graphing libraries with all kinds of features.</i> 

<b>4. Programming applications</b><br> 
<i>You can program all kinds of applications using Python. The general-purpose language can be used to read and create file directories, create GUIs and APIs, and more. Whether it’s blockchain applications, audio and video apps, or machine learning applications, you can build them all with Python.</i> 

<b>5. Web development</b><br>
<i>Python is a great choice for web development. This is largely due to the fact that there are many Python web development frameworks to choose from, such as Django, Pyramid, and Flask. These frameworks have been used to create sites and services such as Spotify, Reddit and Mozilla.</i>

<b>6. Game development</b><br> 
<i>Although far from an industry-standard in game development, Python does have its uses in the industry.</i>

<b>7. Language development</b><br>
<i>The simple and elegant design of Python and its syntax means that it has inspired the creation of new programming languages. Languages such as Cobra, CoffeeScript, and Go all use a similar syntax to Python.</i>

YouTube, Instagram and Quora are among the countless sites that use Python. Much of Dropbox’s code is Python (where Van Rossum works now), Python has been used extensively by digital special effects house ILM (whose work spans across all of the Star Wars and Marvel films) and it’s a favorite of electronics titan Philips
  
# Brief History
Python was created in 1991 by Dutch programmer Guido Van Rossum and was built in a way that it’s relatively intuitive to write and understand, thus enabling it as an ideal coding language for those who want rapid development. 

Many of the biggest organisations in the world implement it in some form. NASA, Google, Netflix, Spotify, and countless more all use the language to help power their services.

Python is the third most popular programming language in the world, behind only Java and C
<br />
<br />
<br />
<br />


# Code
# Variables and Types

Variables are memory locations reserved to store data. The different data types available in Python are Numbers, String, List, Tuple, Dictionary

An excerpt of script used to collect data and store in variables

    # get data
    firstName = input("Please enter first name: ")
    lastName = input("Please enter last name: ")
    emailAddress = input("Please enter email address: ")
    contactNote = input("Any notes?: ")

# IDE
Integatred development Environment: An IDE (or Integrated Development Environment) is a program dedicated to software development. As the name implies, IDEs integrate several tools specifically designed for software development. These tools usually include: An editor designed to handle code (with, for example, syntax highlighting and auto-completion)

# Comments

When writing scripts, we often want to leave ourselves notes or explanations. Python  uses the # character to signify that the line should be ignored and not executed.
                    
    #This is a full line comment 

    or comment at the end of a line of a line of code
                        
    2 + 2 # This will add the numbers

# Variables and Assignment operator

We can assign a value to a variable by using a single = and we don't need to specify the type of the variable.

        my_str = "This is a simple string"
        Now to print this use,
        print(my_str)
        O/P : This is a simple string

We can also change the value that is assigned to a variable later on, either by using the standard assignment operator = or by using some of the short-hand operators.

         my_str += " testing"
         my_str
         'This is a simple string testing'
    Note:
    The contents of a variable can be changed and we don't need to maintain the same type:

        ex:
                my_str = 1
                print(my_str)
                1
                my_str = 1
                my_int = my_str
                my_str = "testing"
                print(my_int)
                1
                print(my_str)
                testing

# Strings
    Strings are immutable sequences of Unicode code points. String literals are written in a variety of ways:
    Single quotes: 'allows embedded "double" quotes'
    Double quotes: "allows embedded 'single' quotes"
    Triple quoted: '''Three single quotes''', """Three double quotes"""

Strings may also be created from other objects using the 'str' constructor.Since there is no separate “character” type, indexing a string produces  strings  of     length 1.      That is, for a non-empty string s, s[0] == s[0:1]

 Strings implement all of the common sequence operations, along with the additional methods described below.

        1. str.capitalize()
            Return a copy of the string with its first character capitalized and the rest lowercased.

        2. str.islower()
            Return True if all cased characters in the string are lowercase and there is at least one cased character, False otherwise.

        3. str.isupper()
             Return True if all cased character in the string are uppercase and there is at least one cased character, False otherwise.

        4. str.lower()
            Return a copy of the string with all the cased characters converted to lowercase.

        5. str.lstrip([chars])
           Return a copy of the string with leading characters removed

        6. str.rstrip([chars])
            Return a copy of the string with trailing characters removed.

        7. str.title()
           Return a titlecased version of the string where words start with an uppercase character and the remaining characters are lowercase.
           
        8. str.upper()
           Return a copy of the string with all the cased characters converted to uppercase

# Logical Operators 

Logical Operators are used to perform certain logical operations on values and variables.

     1.Logical AND
     2.Logical OR
     3.Logical NOT

Logical AND:

        a = 12
        b = 26
        c = 4
        if a > b and a > c: 
        print("Number a is larger") 
        if b > a and b > c: 
        print("Number b is larger") 
        if c > a and c > b:    
        print("Number c is larger") 
        O/P : Number b is larger

 Logical OR:

    a = 10
    b = -5
    if a < 0 or b < 0:
    print("Their product will be negative")
    else:
    print("Their product will be positive")
    O/P : Their product will be negative

Logical NOT :

    a = 10
    if not a == 10:
    print ("a not equals 10")
    else:
    print("a equals 10")
    O/P : a equals 10


# Module:
An external file containing a set of functions you want to include in your program, while keeping that code isolated from other code, usually for better organization of files

Functions can be put into modules. Modules are separate Python files that can be imported into other Python applications. Importing modules makes it possible to reuse code
Libraries are collections of modules 

# Automation:
 The addition of technology that performs tasks with reduced human assistance to processes that facilitate feedback loops between operations and development teams so that iterative updates can be deployed faster to applications in production.

# Configuration Management:
Project Infrastructure refers to the organisational structure, processes, tools, techniques and training an organisation puts in place to make projects more successful.

