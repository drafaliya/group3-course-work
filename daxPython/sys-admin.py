import os
os.system("ls")

import subprocess
subprocess.run(["ls"])

subprocess.run(["ls","-la"])

subprocess.run(["ls","repos"])

command="uname"
commandArgument="-a"
print(f'Gathering system information with command: {command} {commandArgument}')
subprocess.run([command,commandArgument])

command="ps"
commandArgument="-x"
print(f'Gathering active process information with command: {command} {commandArgument}')
subprocess.run([command,commandArgument])