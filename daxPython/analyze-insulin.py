fhand1 = open("preproinsulin-seq.txt", "r")

insulin1 = fhand1.read()
import re

#Cleaning preproinsulin-seq.txt programmatically


result = re.sub("\s|ORIGIN|//|[0-9]|\\n", "",insulin1)

print(result)
length=len(result)
print('The number of character in insulin text file :', length)

fhand2 = open("preproinsulin-seq-clean.txt", "w")
fhand2.write(result)
fhand2.close()

print("Complete excercise 1--------")


file1 = open("preproinsulin-seq-clean.txt", "r")
first_split = file1.read()
print(first_split)
print(len(first_split))
print("__________________________________________")

first_str = first_split[0:24]

char_1_24 = open("lsinsulin-seq-clean.txt", "w")
char_1_24.write(first_str)
char_1_24.close()
print('First 24 character: ', first_str)
print("The length of the first string is:", len(first_str))
print("__________________________________________________")


second_str = first_split[24:54]
char_25_54 = open("binsulin-seq-clean.txt", "w")
char_25_54.write(second_str)
char_25_54.close()
print('number 25 to 54 character: ', second_str)
print("The length of the second string is:", len(second_str))
print("__________________________________________________")

third_str = first_split[54:89]
char_55_89 = open("cinsulin-seq-clean.txt", "w")
char_55_89.write(third_str)
char_55_89.close()
print('number 55 to 89 character: ', third_str)
print("The length of the third string is:", len(third_str))
print("____________________________________________________")

fifth_str = first_split[89:110]
char_89_110 = open("ainsulin-seq-clean.txt", "w")
char_89_110.write(fifth_str)
char_89_110.close()
print('number 90 to 110 character: ', fifth_str)
print("The length of the fifth string is:", len(fifth_str))

