
ages = {'kevin' : 59 , 'alex': 26 , 'bob': 35}
print(ages)

#using dict keyword
weights= dict(kevin=160 , bob=240 , kayla =13)
print(weights)

#using tuples
colors = dict([('kevin', 'blue'),('bob','green'), ('kayla','red')])
print(colors)

#using keys and values
ages = {'kevin' : 59 , 'alex': 26 , 'bob': 35}
print(ages.keys()) #gives just the keys
print(ages.values())  #gives just the value
print(ages.items())   #gives keys and values
print(list(ages.keys()))
print(list(ages.items()))